﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using WpfApplication1.ViewModel;
using WpfApplication1.Model;

namespace WpfApplication1.View
{
    /// <summary>
    /// Логика взаимодействия для AddAuthorView.xaml
    /// </summary>
    public partial class AddAuthorView
    {
        public AddAuthorView(AddAuthorViewModel aavm)
        {
            AddAuthorViewM = aavm;
            InitializeComponent();
        }

        public AddAuthorViewModel AddAuthorViewM { get; }
        

    }
}
