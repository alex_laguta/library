﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WpfApplication1.Model;
using WpfApplication1.ViewModel;

namespace WpfApplication1.ViewModel
{
    public class BookViewModel : ViewModelBase
    {
        private Book _selectedBook;

        public MyCollection<Book> Books { get; set; } = new MyCollection<Book>();

        public BookViewModel()
        {
           /* Books.Add(new Book(9780486269689, 270, 2009, new BookPublishing("Dover Publications", "New York"), "White Fang"));
            Books.Add(new Book(9780486269685, 90, 2013, new BookPublishing("Tom Publications", "New York"), "Light"));
            Books[0].AddAuthor(new BookAuthor("Jack", new DateTime(1876, 03, 23)));
            Books[0].AddAuthor(new BookAuthor("Jack Londn", new DateTime(1876, 03, 23)));
            Books[0].AddAuthor(new BookAuthor("Ja London", new DateTime(1876, 03, 23)));*/
        }

        public Book SelectedBook
        {
            get { return _selectedBook; }
            set
            {
                if (value != null)
                {
                    _selectedBook = value;
                    OnPropertyChanged("SelectedBook");
                };
            }
        }

        public int SelectedSort { get; set; }
    }
}
