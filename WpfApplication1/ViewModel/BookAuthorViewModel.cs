﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WpfApplication1.Model;

namespace WpfApplication1.ViewModel
{
    public class BookAuthorViewModel : ViewModelBase
    {
        public MyCollection<BookAuthor> BookAuthors { get; set; } = new MyCollection<BookAuthor>();
        private BookAuthor _selectedAuthor;

        public BookAuthorViewModel()
        {
           /* BookAuthors.Add(new BookAuthor("Jack London",  new DateTime(1987, 10, 12)));
            BookAuthors.Add(new BookAuthor(" London", new DateTime(1987, 10, 12)));
            BookAuthors[0].AddBook(new Book("gfg", "fgf"));*/
        }

        public BookAuthor SelectedAuthor
        {
            get { return _selectedAuthor; }
            set
            {
                if (value != null)
                {
                    _selectedAuthor = value;
                    OnPropertyChanged("SelectedAuthor");
                };
            }
        }
    }

}
