﻿using System;
using System.Windows.Input;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WpfApplication1.Model;
using WpfApplication1.View;
using System.Collections.Specialized;
using Microsoft.Win32;
using System.IO;
using System.IO.Compression;
using System.Windows;
using System.Windows.Media.Animation;
using System.Windows.Controls;

namespace WpfApplication1.ViewModel
{
    public class MainViewModel : ViewModelBase
    {
        private BookViewModel _bookViewModel;
        private BookAuthorViewModel _bookAuthorViewModel;
        public long sum = 0;

        #region Commands

        public ICommand ClickCommand { get; set; }
        public ICommand ClickRemoveCommand { get; set; }

        public ICommand ClickAddAuthorCommand { get; set; }
        public ICommand ClickRemoveAuthorCommand { get; set; }
        public ICommand ClickRemoveAuthorsCommand { get; set; }
        public ICommand ClickAddNewAuthorCommand { get; set; }
        public ICommand ClickAddImageCommand { get; set; }
        public ICommand ClickAddFileCommand { get; set; }
        public ICommand ClickSaveFileCommand { get; set; }
        public ICommand ClickSaveAsFileCommand { get; set; }
        public ICommand ClickAddBinaryFileCommand { get; set; }
        public ICommand ClickSerializeCommand { get; set; }
        public ICommand ClickDeserializeCommand { get; set; }
        public ICommand SortCommand { get; set; }
        public ICommand NarrowCommand { get; set; }
        public ICommand SumChangedCommand { get; set; }
        public ICommand ClickAnimationCommand { get; set; }

        #endregion

        public BookViewModel BookViewModel
        {
            get { return _bookViewModel; }
            set
            {
                _bookViewModel = value;
                OnPropertyChanged("BookViewModel");

            }

        }

        public BookAuthorViewModel BookAuthorViewModel
        {
            get { return _bookAuthorViewModel; }
            set
            {
                _bookAuthorViewModel = value;
                OnPropertyChanged("BookAuthorViewModel");
            }
        }

        public void AddNewBook()
        {
            Book NewBook = new Book("Untitled", "Unknown");
            BookViewModel.Books.Add(NewBook);
            BookViewModel.SelectedBook = NewBook;
        }

        public void RemoveBook()
        {
            int i = BookViewModel.Books.IndexOf(BookViewModel.SelectedBook);
            BookViewModel.SelectedBook = BookViewModel.Books[0];
            BookViewModel.Books.RemoveAt(i);
        }

        public void AddAuthor()
        {
            AddAuthorViewModel AddAuthorWindowModel = new AddAuthorViewModel(this, BookViewModel.Books.IndexOf(BookViewModel.SelectedBook));
            var a = new AddAuthorView(AddAuthorWindowModel);
            a.Show(); 
        }

        public void RemoveAuthor()
        {
            int i = BookViewModel.SelectedBook.AuthorsList.IndexOf(BookAuthorViewModel.SelectedAuthor);
            if (i == -1) return;
            if (BookAuthorViewModel.BookAuthors.IndexOf(BookAuthorViewModel.SelectedAuthor) != -1)
            BookAuthorViewModel.BookAuthors[BookAuthorViewModel.BookAuthors.IndexOf(BookAuthorViewModel.SelectedAuthor)].BooksList.RemoveAt
                (BookAuthorViewModel.BookAuthors[BookAuthorViewModel.BookAuthors.IndexOf(BookAuthorViewModel.SelectedAuthor)].
                BooksList.IndexOf(BookViewModel.SelectedBook));
            BookAuthorViewModel.SelectedAuthor = BookViewModel.SelectedBook.AuthorsList[0];
            BookViewModel.SelectedBook.AuthorsList.RemoveAt(i);
        }

        public void RemoveAuthors()
        {
            int i = BookAuthorViewModel.BookAuthors.IndexOf(BookAuthorViewModel.SelectedAuthor);
            BookAuthorViewModel.SelectedAuthor = BookAuthorViewModel.BookAuthors[0];
            BookAuthorViewModel.BookAuthors.RemoveAt(i);
        }

        public void AddNewAuthor()
        {
            BookAuthor NewAuthor = new BookAuthor("Unnamed", new DateTime(1900, 01, 01), "D:\\unknown.jpg");
            BookAuthorViewModel.BookAuthors.Add(NewAuthor);
            BookAuthorViewModel.SelectedAuthor = BookAuthorViewModel.BookAuthors[BookAuthorViewModel.BookAuthors.Count - 1];
        }

        public void AddImage()
        {
            OpenFileDialog dlg = new OpenFileDialog();
            dlg.InitialDirectory = "c:\\";
            dlg.Filter = "Image files (*.jpg)|*.jpg|All Files (*.*)|*.*";
            dlg.RestoreDirectory = true;

            if (dlg.ShowDialog() == true)
            {
                BookAuthorViewModel.SelectedAuthor.Photo = dlg.FileName;
            }
        }

        #region FileMethods

        public void AddFile()
        {
            string path = null;
            OpenFileDialog dlg = new OpenFileDialog();
            dlg.InitialDirectory = "d:\\files";
            dlg.Filter = "Text Files (*.txt*)|*.txt*";
            dlg.RestoreDirectory = true;

            if (dlg.ShowDialog() == true)
            {
                path = dlg.FileName;
            }
            if (path == "")
                return;
            FileInfo file = new FileInfo(path);
            FileStream fs = file.Open(FileMode.Open, FileAccess.Read);
            StreamReader reader = new StreamReader(fs);

            int i = 0;
            MyCollection<Book> newBooks = new MyCollection<Book>();
            while (reader.EndOfStream == false)
            {
                string name = reader.ReadLine();
                long ISBN = long.Parse(reader.ReadLine());
                int pagesQuantity = Int32.Parse(reader.ReadLine());
                string pName = reader.ReadLine();
                int publishingYear = Int32.Parse(reader.ReadLine());
                string pCity = reader.ReadLine();
                newBooks.Add(new Book(ISBN, pagesQuantity, publishingYear, new BookPublishing(pName, pCity), name));
                if (BookViewModel.Books.IndexOf(newBooks[i]) == -1)
                {
                    BookViewModel.Books.Add(newBooks[i]);
                }
           
                string s = null;
                while ((s = reader.ReadLine()) != "EndAList")
                {
                    string aName = s;
                    string aPhoto = reader.ReadLine();
                    DateTime aBirthDate = DateTime.Parse(reader.ReadLine());
                    BookAuthor newAuthor = new BookAuthor(aName, aBirthDate, aPhoto);
                    if (!BookAuthorViewModel.BookAuthors.Contains(newAuthor))
                    {
                        BookAuthorViewModel.BookAuthors.Add(newAuthor);
                    }
                    if (!BookViewModel.Books[BookViewModel.Books.IndexOf(newBooks[i])].AuthorsList.Contains(newAuthor)) 
                    BookViewModel.Books[BookViewModel.Books.IndexOf(newBooks[i])].AddAuthor
                                  (BookAuthorViewModel.BookAuthors[BookAuthorViewModel.BookAuthors.IndexOf(newAuthor)]);
                    if (!BookAuthorViewModel.BookAuthors[BookAuthorViewModel.BookAuthors.IndexOf(newAuthor)].BooksList.Contains(newBooks[i]))
                    BookAuthorViewModel.BookAuthors[BookAuthorViewModel.BookAuthors.IndexOf(newAuthor)].AddBook
                                  (BookViewModel.Books[BookViewModel.Books.IndexOf(newBooks[i])]);
                }
                i++;
            }

        }

        public void SaveFile()
        {
            string path = null;
            SaveFileDialog dlg = new SaveFileDialog(); 
            dlg.InitialDirectory = "d:\\files";
            dlg.Filter = "Text Files (*.txt*)|*.txt*";
            dlg.RestoreDirectory = true;

            dlg.ShowDialog();
            path = dlg.FileName;

            if (path == "")
                return;
            FileInfo file = new FileInfo(path);
            FileStream fs = file.Open(FileMode.OpenOrCreate, FileAccess.Write);
            StreamWriter writer = new StreamWriter(fs);
            int j, i = 0;
            while(i < BookViewModel.Books.Count)
              {
                  writer.WriteLine(BookViewModel.Books[i].Name);
                  writer.WriteLine(BookViewModel.Books[i].ISBN);
                  writer.WriteLine(BookViewModel.Books[i].PagesQuantity);
                  writer.WriteLine(BookViewModel.Books[i].PublishingYear);
                  writer.WriteLine(BookViewModel.Books[i].Publisher.Name);
                  writer.WriteLine(BookViewModel.Books[i].Publisher.City);
                  j = 0;
                  while(j < BookViewModel.Books[i].AuthorsList.Count)
                  {
                      writer.WriteLine(BookViewModel.Books[i].AuthorsList[j].Name);
                      writer.WriteLine(BookViewModel.Books[i].AuthorsList[j].Photo);
                      writer.WriteLine(BookViewModel.Books[i].AuthorsList[j].BirthDate);
                      j++;
                  }
                  writer.WriteLine("EndAList");
                  i++;
              }
            writer.Flush();
            writer.Close();
        }

        public void SaveAsBinaryFile()
        {
            string path = null;
            SaveFileDialog dlg = new SaveFileDialog();
            dlg.InitialDirectory = "d:\\files";
            dlg.Filter = "All Files(*.*) | *.*";
            dlg.RestoreDirectory = true;

            dlg.ShowDialog();
            path = dlg.FileName;

            if (path == "")
                return;
            FileInfo file = new FileInfo(path);
            FileStream fs = file.Open(FileMode.OpenOrCreate, FileAccess.Write);
            BinaryWriter writer = new BinaryWriter(fs);
            int j, i = 0;
            while (i < BookViewModel.Books.Count)
            {
                writer.Write(BookViewModel.Books[i].Name);
                writer.Write(BookViewModel.Books[i].ISBN);
                writer.Write(BookViewModel.Books[i].PagesQuantity);
                writer.Write(BookViewModel.Books[i].PublishingYear);
                writer.Write(BookViewModel.Books[i].Publisher.Name);
                writer.Write(BookViewModel.Books[i].Publisher.City);
                j = 0;
                while (j < BookViewModel.Books[i].AuthorsList.Count)
                {
                    writer.Write(BookViewModel.Books[i].AuthorsList[j].Name);
                    writer.Write(BookViewModel.Books[i].AuthorsList[j].Photo);
                    writer.Write(BookViewModel.Books[i].AuthorsList[j].BirthDate.ToBinary());
                    j++;
                }
                writer.Write("EndAList");
                i++;
            }
            writer.Flush();
            writer.Close();
            Compress(file);
            file.Delete();
        }

        public void AddAsBinaryFile()
        {
            string path = null;
            OpenFileDialog dlg = new OpenFileDialog();
            dlg.InitialDirectory = "d:\\files";
            dlg.Filter = "All Files(*.*) | *.*";
            dlg.RestoreDirectory = true;

            if (dlg.ShowDialog() == true)
            {
                path = dlg.FileName;
            }
            if (path == "")
                return;
            FileInfo file = new FileInfo(path);
            if (file.Extension == ".gz")
            {
                Decompress(file);
                FileInfo newFile = new FileInfo(path.Remove(path.Length-3));
                file.Delete();
                file = newFile;

            }
            FileStream fs = file.Open(FileMode.Open, FileAccess.Read);
            BinaryReader reader = new BinaryReader(fs);

            int i = 0;
            MyCollection<Book> newBooks = new MyCollection<Book>();
            while (reader.PeekChar() != -1)
            {
                string name = reader.ReadString();
                long ISBN = reader.ReadInt64();
                int pagesQuantity = reader.ReadInt32();
                int publishingYear = reader.ReadInt32();
                string pName = reader.ReadString();
                string pCity = reader.ReadString();
                newBooks.Add(new Book(ISBN, pagesQuantity, publishingYear, new BookPublishing(pName, pCity), name));
                if (BookViewModel.Books.IndexOf(newBooks[i]) == -1)
                {
                    BookViewModel.Books.Add(newBooks[i]);
                }

                string s = null;
                while ((s = reader.ReadString()) != "EndAList")
                {
                    string aName = s;
                    string aPhoto = reader.ReadString();
                    DateTime aBirthDate = DateTime.FromBinary(reader.ReadInt64());
                    BookAuthor newAuthor = new BookAuthor(aName, aBirthDate, aPhoto);
                    if (!BookAuthorViewModel.BookAuthors.Contains(newAuthor))
                    {
                        BookAuthorViewModel.BookAuthors.Add(newAuthor);
                    }
                    if (!BookViewModel.Books[BookViewModel.Books.IndexOf(newBooks[i])].AuthorsList.Contains(newAuthor))
                        BookViewModel.Books[BookViewModel.Books.IndexOf(newBooks[i])].AddAuthor
                                      (BookAuthorViewModel.BookAuthors[BookAuthorViewModel.BookAuthors.IndexOf(newAuthor)]);
                    if (!BookAuthorViewModel.BookAuthors[BookAuthorViewModel.BookAuthors.IndexOf(newAuthor)].BooksList.Contains(newBooks[i]))
                        BookAuthorViewModel.BookAuthors[BookAuthorViewModel.BookAuthors.IndexOf(newAuthor)].AddBook
                                      (BookViewModel.Books[BookViewModel.Books.IndexOf(newBooks[i])]);
                }
                i++;
            }
        }

        #endregion FileMethods

        #region Compression
        public static void Compress(FileInfo fileToCompress)
        {
            using (FileStream originalFileStream = fileToCompress.OpenRead())
            {
                if ((File.GetAttributes(fileToCompress.FullName) &
                   FileAttributes.Hidden) != FileAttributes.Hidden & fileToCompress.Extension != ".gz")
                {
                    using (FileStream compressedFileStream = File.Create(fileToCompress.FullName + ".gz"))
                    {
                        using (GZipStream compressionStream = new GZipStream(compressedFileStream,
                           CompressionMode.Compress))
                        {
                            originalFileStream.CopyTo(compressionStream);
                        }
                    }
                }
            }
        }

        public static void Decompress(FileInfo fileToDecompress)
        {
            using (FileStream originalFileStream = fileToDecompress.OpenRead())
            {
                string currentFileName = fileToDecompress.FullName;
                string newFileName = currentFileName.Remove(currentFileName.Length - fileToDecompress.Extension.Length);

                using (FileStream decompressedFileStream = File.Create(newFileName))
                {
                    using (GZipStream decompressionStream = new GZipStream(originalFileStream, CompressionMode.Decompress))
                    {
                        decompressionStream.CopyTo(decompressedFileStream);
                    }
                }
            }
        }
        #endregion Compression


        public void Serialize()
        {
            SaveFileDialog fileDialog = new SaveFileDialog();
            string filename;

            fileDialog.DefaultExt = ".txt";
            fileDialog.Filter = "Text files (txt)|*.txt";
            if (fileDialog.ShowDialog() == true)
                filename = fileDialog.FileName;
            else
                return;
            Serializer sl = new Serializer(filename);
            sl.SerializeObject(BookViewModel.Books);
            sl.Close();
            
        }

        public void Deserialize()
        {
            OpenFileDialog fileDialog = new OpenFileDialog();
            string filename;

            fileDialog.DefaultExt = ".txt";
            fileDialog.Filter = "Text documents (txt)|*.txt";
            if (fileDialog.ShowDialog() == true)
                filename = fileDialog.FileName;
            else
                return;
            Serializer sl = new Serializer(filename);
            MyCollection<Book> newBooks = (MyCollection<Book>)sl.DeserializeObject();
            BookViewModel.Books.Clear();
            newBooks.CopyTo(BookViewModel.Books);
            UpdateAuthors();
            sl.Close();
        }

        public void Sort()
        {
            IEnumerable<Book> newList;
            if (BookViewModel.SelectedSort == 0)
                newList = BookViewModel.Books.OrderBy(book => book.Name);
            else if (BookViewModel.SelectedSort == 1)
                newList = BookViewModel.Books.OrderBy(book => book.ISBN);
                 else if (BookViewModel.SelectedSort == 2)
                newList = BookViewModel.Books.OrderBy(book => book.PagesQuantity);
                      else if (BookViewModel.SelectedSort == 3)
                newList = BookViewModel.Books.OrderBy(book => book.PublishingYear);
                           else newList = BookViewModel.Books.OrderBy(book => book.Publisher.Name);
            int i = 0;
            foreach (Book sBook in newList)
            {
                BookViewModel.Books.MoveTo(sBook, i);
                i++;
            }
            UpdateAuthors();
        }

        public void Narrow()
        {
            IEnumerable<Book> newList =  BookViewModel.Books.Where(book => (book.PagesQuantity > 100 && book.PublishingYear > 2000));
            foreach (Book sBook in BookViewModel.Books)
            {
                if (newList.Contains(sBook) == false)
                BookViewModel.Books.Remove(sBook);
            }
        }

        public void SumChanged()
        {
            sum = 0;
            sum = BookViewModel.Books.Aggregate(0, (sum, book) => sum + book.PagesQuantity);
            if (sum > 1000)
            {
                MessageBoxResult result = MessageBox.Show("The sum of quantity of pages is more than 500!", "Message", MessageBoxButton.OK, MessageBoxImage.Question);
            }
        }

        public void UpdateAuthors()
        {
            BookAuthorViewModel.BookAuthors.Clear();
            foreach (Book book in BookViewModel.Books)
            {
                foreach(BookAuthor author in book.AuthorsList)
                {
                    if (BookAuthorViewModel.BookAuthors.Contains(author) == false)
                    {
                        BookAuthorViewModel.BookAuthors.Add(author);
                    }
                    if (BookAuthorViewModel.BookAuthors[BookAuthorViewModel.BookAuthors.IndexOf(author)].BooksList.Contains(book) == false)
                    BookAuthorViewModel.BookAuthors[BookAuthorViewModel.BookAuthors.IndexOf(author)].BooksList.Add(book);
                }
            }
        }

        public void AnimationView()
        {
            var a = new Graph();
            a.Show();
        }

        public MainViewModel()
        {
            ClickCommand = new Command(arg => AddNewBook());
            ClickRemoveCommand = new Command(arg => RemoveBook());
            ClickAddAuthorCommand = new Command(arg => AddAuthor());
            ClickRemoveAuthorCommand = new Command(arg => RemoveAuthor());
            ClickRemoveAuthorsCommand = new Command(arg => RemoveAuthors());
            ClickAddNewAuthorCommand = new Command(arg => AddNewAuthor());
            ClickAddImageCommand = new Command(arg => AddImage());
            ClickAddFileCommand = new Command(arg => AddFile());
            ClickAddBinaryFileCommand = new Command(arg => AddAsBinaryFile());
            ClickSaveFileCommand = new Command(arg => SaveFile());
            ClickSaveAsFileCommand = new Command(arg => SaveAsBinaryFile());
            ClickSerializeCommand = new Command(arg => Serialize());
            ClickDeserializeCommand = new Command(arg => Deserialize());
            SortCommand = new Command(arg => Sort());
            NarrowCommand = new Command(arg => Narrow());
            SumChangedCommand = new Command(arg => SumChanged());
            ClickAnimationCommand = new Command(arg => AnimationView());

            BookViewModel = new BookViewModel();
            BookAuthorViewModel = new BookAuthorViewModel();

            BookViewModel.Books.Add(new Book(9781402725005, 160, 2006, new BookPublishing("Sterling Publishing", "New York"), "White Fang"));
            BookAuthorViewModel.BookAuthors.Add(new BookAuthor("Jack London", new DateTime(1876, 01, 12), "D:\\book\\london.jpg"));
            BookViewModel.Books[0].AuthorsList.Add(BookAuthorViewModel.BookAuthors[0]);
            BookAuthorViewModel.BookAuthors[0].BooksList.Add(BookViewModel.Books[0]);

            BookViewModel.Books.Add(new Book(9780751565355, 352, 2016, new BookPublishing("Little Brown", "London"), "Harry Potter & the Cursed Child"));
            BookAuthorViewModel.BookAuthors.Add(new BookAuthor("J. K. Rowling", new DateTime(1965, 07, 31), "D:\\book\\rowling.jpg"));
            BookViewModel.Books[1].AuthorsList.Add(BookAuthorViewModel.BookAuthors[1]);
            BookAuthorViewModel.BookAuthors[1].BooksList.Add(BookViewModel.Books[1]);
        }
    }
}
