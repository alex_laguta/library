﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using WpfApplication1.Model;
using WpfApplication1.View;

namespace WpfApplication1.ViewModel
{
    public class AddAuthorViewModel : ViewModelBase
    {
        private string _name;

        public string Name
        {
            get { return _name; }

            set { _name = value; }
        }
        public DateTime birthDate { get; set; }
        public string photo { get; set; }

        public MainViewModel myModel { get; }
        public int index { get; }

        public AddAuthorViewModel (MainViewModel model, int i)
        {
            myModel = model;
            index = i;
            ClickOKCommand = new Command(arg => AddAuthor());
            ClickAddEAuthorCommand = new Command(arg => AddExAuthor());
        }

        public void AddAuthor()
        {
            BookAuthor author = new BookAuthor(Name, birthDate, photo = "D:\\unknown.jpg");
            myModel.BookAuthorViewModel.BookAuthors.Add(author);
            int i = myModel.BookAuthorViewModel.BookAuthors.IndexOf(author);
            myModel.BookViewModel.Books[index].AddAuthor(myModel.BookAuthorViewModel.BookAuthors[i]);
            myModel.BookAuthorViewModel.BookAuthors[i].AddBook(myModel.BookViewModel.Books[index]);

        }

        public void AddExAuthor()
        {
            if (myModel.BookViewModel.Books[index].AuthorsList.IndexOf(myModel.BookAuthorViewModel.SelectedAuthor) == -1)
            {
                myModel.BookViewModel.Books[index].AddAuthor(myModel.BookAuthorViewModel.SelectedAuthor);
                int i = myModel.BookAuthorViewModel.BookAuthors.IndexOf(myModel.BookAuthorViewModel.SelectedAuthor);
                myModel.BookAuthorViewModel.BookAuthors[i].AddBook(myModel.BookViewModel.Books[index]);
            }
        }

        public ICommand ClickOKCommand { get; set; }

        public ICommand ClickAddEAuthorCommand { get; set; }

    }
}
