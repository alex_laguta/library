﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Reflection;

namespace WpfApplication1
{
    class Serializer
    {
        public FileStream stream = null;
        protected StreamWriter writer = null;
        protected StreamReader reader = null;
        protected int spaces = 0;

        public Serializer(string filename)
        {
                stream = new FileStream(filename, FileMode.OpenOrCreate);
                writer = new StreamWriter(stream);
                reader = new StreamReader(stream);
        }

        public void Close()
        {
                writer.Close();
                reader.Close();
                stream.Close();
        }

        protected void WriteLine(string key, string line, bool convert)
        {
            if (spaces > 0)
                writer.Write(new string(' ', spaces << 1));

            if (convert)
            {
                line = line.Replace("&", "&amp;");
                line = line.Replace("\n", "&ln;");
                line = line.Replace("\r", "&lf;");
                line = line.Replace("{", "&bl;");
                line = line.Replace("}", "&br;");
                line = line.Replace("[", "&al;");
                line = line.Replace("]", "&ar;");
                line = line.Replace("(", "&ul;");
                line = line.Replace(")", "&ur;");
                line = line.Replace("=", "&eq;");
            }

            if (key != null)
                writer.Write(key + "=");

            writer.WriteLine(line);
        }

        protected string ReadLine(out string key, bool convert)
        {
            string line;

            while ((line = reader.ReadLine()) != null)
            {
                if (line.Trim().Length > 0)
                    break;
            }

            if (line != null)
            {
                int pos = (line = line.TrimStart(null)).IndexOf('=');

                if (pos > 0)
                {
                    key = line.Substring(0, pos);
                    line = line.Substring(pos + 1);
                }
                else key = null;

                if (convert)
                {
                    line = line.Replace("&amp;", "&");
                    line = line.Replace("&ln;", "\n");
                    line = line.Replace("&lf;", "\r");
                    line = line.Replace("&bl;", "{");
                    line = line.Replace("&br;", "}");
                    line = line.Replace("&al;", "[");
                    line = line.Replace("&ar;", "]");
                    line = line.Replace("&ul;", "(");
                    line = line.Replace("&ur;", ")");
                    line = line.Replace("&eq;", "=");
                }
            }
            else key = null;

            return line;
        }

        public void SerializeObject(object myObject)
        {
            List<object> sMyCollection = new List<object>();
            SerializeObject(myObject, sMyCollection);
        }

        protected void SerializeObject(object myObject, List<object> sMyCollection)
        {
            int refindex;

            if (myObject == null)
            {
                WriteLine(null, typeof(object).FullName, true);
                WriteLine(null, "{", false);
            }
            else if ((refindex = sMyCollection.IndexOf(myObject)) >= 0)
            {
                Type objtype = myObject.GetType();
                WriteLine(null, objtype.FullName, true);
                WriteLine(null, "{", false);
                spaces++;
                WriteLine("index", refindex.ToString(), false);
                spaces--;
            }
            else
            {
                sMyCollection.Add(myObject);

                Type objtype = myObject.GetType();

                WriteLine(null, objtype.FullName, true);
                WriteLine(null, "{", false);
                spaces++;

                if (objtype.IsPrimitive || objtype.Equals(typeof(string)))
                {
                    WriteLine("value", myObject.ToString(), true);
                }
                else if (objtype.IsArray)
                {
                    WriteLine(null, "[", false);
                    spaces++;

                    WriteLine("count", ((System.Array)myObject).Length.ToString(), false);
                    WriteLine(null, "", false);

                    foreach (object item in ((System.Array)myObject))
                    {
                        SerializeObject(item, sMyCollection);
                    }

                    spaces--;
                    WriteLine(null, "]", false);
                }
                else
                {
                    WriteLine(null, "[", false);
                    spaces++;

                    foreach (FieldInfo finfo in objtype.GetFields(BindingFlags.GetField | BindingFlags.SetField | 
                        BindingFlags.NonPublic | BindingFlags.Public | BindingFlags.Instance))
                    {
                        if (!finfo.IsLiteral && !finfo.IsInitOnly && !finfo.IsNotSerialized 
                            && finfo.FieldType.BaseType != null && !finfo.FieldType.BaseType.Equals(typeof(System.MulticastDelegate)))
                        {
                            WriteLine(null, finfo.Name, true);
                            WriteLine(null, "(", false);
                            spaces++;
                            SerializeObject(finfo.GetValue(myObject), sMyCollection);
                            spaces--;
                            WriteLine(null, ")", false);
                        }
                    }

                    spaces--;
                    WriteLine(null, "]", false);
                }

                spaces--;
            }

            WriteLine(null, "}", false);
        }

        public object DeserializeObject()
        {
            List<object> sMyCollection = new List<object>();
            return DeserializeObject(null, sMyCollection);
        }

        protected object DeserializeObject(string className, List<object> sMyCollection)
        {
            object rObject = null;
            string key = null;
            string line;

            if (className == null)
                className = ReadLine(out key, true);

            if (className == null)
                return null;

            if (key != null || ReadLine(out key, false) != "{")
                throw new Exception("Bad header object signature");

            if ((line = ReadLine(out key, true)) != "}")
            {
                Type objtype = Type.GetType(className);
                if (key != null && key == "index")
                {
                    int refindex = Convert.ToInt32(line);

                    if (refindex < 0 || refindex >= sMyCollection.Count)
                        throw new Exception("Bad index on previous object");

                    rObject = sMyCollection[refindex];
                }
                else if (objtype.IsPrimitive)
                {
                    if (key != "value")
                        throw new Exception("Bad value for primitive object");

                    rObject = (object)objtype.InvokeMember("Parse",
                      BindingFlags.InvokeMethod | BindingFlags.Static | BindingFlags.Public,
                      null, null, new object[] { line });

                    sMyCollection.Add(rObject);
                }
                else if (objtype.Equals(typeof(string)))
                {
                    if (key != "value")
                        throw new Exception("Bad value for primitive object");

                    rObject = line;
                    sMyCollection.Add(rObject);
                }
                else if (line == "[" && key == null)
                {
                    if (objtype.IsArray)
                    {
                        if ((line = ReadLine(out key, false)) == null || key != "count")
                            throw new Exception("Bad count for array object");

                        int count = Convert.ToInt32(line), index = 0;

                        rObject = Activator.CreateInstance(objtype, count);
                        sMyCollection.Add(rObject);

                        while ((line = ReadLine(out key, true)) != "]" || key != null)
                            ((System.Array)rObject).SetValue(DeserializeObject(line, sMyCollection), index++);
                    }
                    else
                    {
                        rObject = Activator.CreateInstance(objtype);
                        sMyCollection.Add(rObject);

                        while ((line = ReadLine(out key, true)) != "]" || key != null)
                        {
                            if (line == null || key != null)
                                throw new Exception("Bad header field signature");

                            string fieldname = line;

                            if ((line = ReadLine(out key, true)) != "(" || key != null)
                                throw new Exception("Bad header object signature");

                            object fieldobj = DeserializeObject(null, sMyCollection);
                            FieldInfo fiendinf = objtype.GetField(fieldname, BindingFlags.GetField
                                | BindingFlags.SetField | BindingFlags.NonPublic | BindingFlags.Public | BindingFlags.Instance);

                            if (fiendinf != null)
                                fiendinf.SetValue(rObject, fieldobj);

                            if ((line = ReadLine(out key, true)) != ")" || key != null)
                                throw new Exception("Bad tail object signature");
                        }
                    }
                }
                else throw new Exception("Bad header content signature");

                if ((line = ReadLine(out key, true)) != "}" || key != null)
                    throw new Exception("Bad tail object signature");
            }
            else if (key != null)
                throw new Exception("Bad tail object signature");

            return rObject;
        }
    }
}

