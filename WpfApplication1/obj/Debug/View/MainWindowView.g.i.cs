﻿#pragma checksum "..\..\..\View\MainWindowView.xaml" "{406ea660-64cf-4c82-b6f0-42d48172a799}" "410F00C0887A75EC960B3CA7002C9972"
//------------------------------------------------------------------------------
// <auto-generated>
//     Этот код создан программой.
//     Исполняемая версия:4.0.30319.42000
//
//     Изменения в этом файле могут привести к неправильной работе и будут потеряны в случае
//     повторной генерации кода.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Shell;
using WpfApplication1.Model;
using WpfApplication1.Properties;
using WpfApplication1.View;


namespace WpfApplication1.View {
    
    
    /// <summary>
    /// MainWindowView
    /// </summary>
    public partial class MainWindowView : System.Windows.Window, System.Windows.Markup.IComponentConnector {
        
        
        #line 42 "..\..\..\View\MainWindowView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Image OpenIcon;
        
        #line default
        #line hidden
        
        
        #line 47 "..\..\..\View\MainWindowView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Image Open2Icon;
        
        #line default
        #line hidden
        
        
        #line 52 "..\..\..\View\MainWindowView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Image Save1Icon;
        
        #line default
        #line hidden
        
        
        #line 57 "..\..\..\View\MainWindowView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Image Save2Icon;
        
        #line default
        #line hidden
        
        
        #line 62 "..\..\..\View\MainWindowView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Image Open3Icon;
        
        #line default
        #line hidden
        
        
        #line 67 "..\..\..\View\MainWindowView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Image Save3Icon;
        
        #line default
        #line hidden
        
        
        #line 73 "..\..\..\View\MainWindowView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TabControl tabControl;
        
        #line default
        #line hidden
        
        
        #line 81 "..\..\..\View\MainWindowView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ComboBox comboBox;
        
        #line default
        #line hidden
        
        
        #line 82 "..\..\..\View\MainWindowView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox textBoxName;
        
        #line default
        #line hidden
        
        
        #line 83 "..\..\..\View\MainWindowView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label labelName;
        
        #line default
        #line hidden
        
        
        #line 84 "..\..\..\View\MainWindowView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox textBoxISBN;
        
        #line default
        #line hidden
        
        
        #line 85 "..\..\..\View\MainWindowView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label labelISBN;
        
        #line default
        #line hidden
        
        
        #line 86 "..\..\..\View\MainWindowView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox textBoxPagesQuantity;
        
        #line default
        #line hidden
        
        
        #line 91 "..\..\..\View\MainWindowView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label labelPagesQuantity;
        
        #line default
        #line hidden
        
        
        #line 92 "..\..\..\View\MainWindowView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ListBox listBoxAuthors;
        
        #line default
        #line hidden
        
        
        #line 102 "..\..\..\View\MainWindowView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label labelYear;
        
        #line default
        #line hidden
        
        
        #line 103 "..\..\..\View\MainWindowView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox textYear;
        
        #line default
        #line hidden
        
        
        #line 104 "..\..\..\View\MainWindowView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label labelPublisher;
        
        #line default
        #line hidden
        
        
        #line 105 "..\..\..\View\MainWindowView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox textPublisher;
        
        #line default
        #line hidden
        
        
        #line 106 "..\..\..\View\MainWindowView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label labelBookAuthor;
        
        #line default
        #line hidden
        
        
        #line 107 "..\..\..\View\MainWindowView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button buttonAddAuthor;
        
        #line default
        #line hidden
        
        
        #line 108 "..\..\..\View\MainWindowView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button buttonRemoveAuthor;
        
        #line default
        #line hidden
        
        
        #line 109 "..\..\..\View\MainWindowView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button buttonAddBook;
        
        #line default
        #line hidden
        
        
        #line 110 "..\..\..\View\MainWindowView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button buttonRemoveBook;
        
        #line default
        #line hidden
        
        
        #line 111 "..\..\..\View\MainWindowView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button buttonAnimation;
        
        #line default
        #line hidden
        
        
        #line 120 "..\..\..\View\MainWindowView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button buttonNarrowBook;
        
        #line default
        #line hidden
        
        
        #line 121 "..\..\..\View\MainWindowView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ListView BooksListView;
        
        #line default
        #line hidden
        
        
        #line 141 "..\..\..\View\MainWindowView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button buttonAddImage;
        
        #line default
        #line hidden
        
        
        #line 142 "..\..\..\View\MainWindowView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox AuthorName;
        
        #line default
        #line hidden
        
        
        #line 143 "..\..\..\View\MainWindowView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label labelName1;
        
        #line default
        #line hidden
        
        
        #line 144 "..\..\..\View\MainWindowView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox textBoxBirth;
        
        #line default
        #line hidden
        
        
        #line 145 "..\..\..\View\MainWindowView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label labelBirth;
        
        #line default
        #line hidden
        
        
        #line 146 "..\..\..\View\MainWindowView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label listBooks;
        
        #line default
        #line hidden
        
        
        #line 147 "..\..\..\View\MainWindowView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button buttonAddNewAuthor;
        
        #line default
        #line hidden
        
        
        #line 148 "..\..\..\View\MainWindowView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button buttonRemoveAuthors;
        
        #line default
        #line hidden
        
        
        #line 149 "..\..\..\View\MainWindowView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ListBox listBoxBooks;
        
        #line default
        #line hidden
        
        
        #line 150 "..\..\..\View\MainWindowView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ListView BooksAuthorsListView;
        
        #line default
        #line hidden
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Uri resourceLocater = new System.Uri("/WpfApplication1;component/view/mainwindowview.xaml", System.UriKind.Relative);
            
            #line 1 "..\..\..\View\MainWindowView.xaml"
            System.Windows.Application.LoadComponent(this, resourceLocater);
            
            #line default
            #line hidden
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 1:
            this.OpenIcon = ((System.Windows.Controls.Image)(target));
            return;
            case 2:
            this.Open2Icon = ((System.Windows.Controls.Image)(target));
            return;
            case 3:
            this.Save1Icon = ((System.Windows.Controls.Image)(target));
            return;
            case 4:
            this.Save2Icon = ((System.Windows.Controls.Image)(target));
            return;
            case 5:
            this.Open3Icon = ((System.Windows.Controls.Image)(target));
            return;
            case 6:
            this.Save3Icon = ((System.Windows.Controls.Image)(target));
            return;
            case 7:
            this.tabControl = ((System.Windows.Controls.TabControl)(target));
            return;
            case 8:
            this.comboBox = ((System.Windows.Controls.ComboBox)(target));
            return;
            case 9:
            this.textBoxName = ((System.Windows.Controls.TextBox)(target));
            return;
            case 10:
            this.labelName = ((System.Windows.Controls.Label)(target));
            return;
            case 11:
            this.textBoxISBN = ((System.Windows.Controls.TextBox)(target));
            return;
            case 12:
            this.labelISBN = ((System.Windows.Controls.Label)(target));
            return;
            case 13:
            this.textBoxPagesQuantity = ((System.Windows.Controls.TextBox)(target));
            return;
            case 14:
            this.labelPagesQuantity = ((System.Windows.Controls.Label)(target));
            return;
            case 15:
            this.listBoxAuthors = ((System.Windows.Controls.ListBox)(target));
            return;
            case 16:
            this.labelYear = ((System.Windows.Controls.Label)(target));
            return;
            case 17:
            this.textYear = ((System.Windows.Controls.TextBox)(target));
            return;
            case 18:
            this.labelPublisher = ((System.Windows.Controls.Label)(target));
            return;
            case 19:
            this.textPublisher = ((System.Windows.Controls.TextBox)(target));
            return;
            case 20:
            this.labelBookAuthor = ((System.Windows.Controls.Label)(target));
            return;
            case 21:
            this.buttonAddAuthor = ((System.Windows.Controls.Button)(target));
            return;
            case 22:
            this.buttonRemoveAuthor = ((System.Windows.Controls.Button)(target));
            return;
            case 23:
            this.buttonAddBook = ((System.Windows.Controls.Button)(target));
            return;
            case 24:
            this.buttonRemoveBook = ((System.Windows.Controls.Button)(target));
            return;
            case 25:
            this.buttonAnimation = ((System.Windows.Controls.Button)(target));
            return;
            case 26:
            this.buttonNarrowBook = ((System.Windows.Controls.Button)(target));
            return;
            case 27:
            this.BooksListView = ((System.Windows.Controls.ListView)(target));
            return;
            case 28:
            this.buttonAddImage = ((System.Windows.Controls.Button)(target));
            return;
            case 29:
            this.AuthorName = ((System.Windows.Controls.TextBox)(target));
            return;
            case 30:
            this.labelName1 = ((System.Windows.Controls.Label)(target));
            return;
            case 31:
            this.textBoxBirth = ((System.Windows.Controls.TextBox)(target));
            return;
            case 32:
            this.labelBirth = ((System.Windows.Controls.Label)(target));
            return;
            case 33:
            this.listBooks = ((System.Windows.Controls.Label)(target));
            return;
            case 34:
            this.buttonAddNewAuthor = ((System.Windows.Controls.Button)(target));
            return;
            case 35:
            this.buttonRemoveAuthors = ((System.Windows.Controls.Button)(target));
            return;
            case 36:
            this.listBoxBooks = ((System.Windows.Controls.ListBox)(target));
            return;
            case 37:
            this.BooksAuthorsListView = ((System.Windows.Controls.ListView)(target));
            return;
            }
            this._contentLoaded = true;
        }
    }
}

