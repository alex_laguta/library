﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;

namespace WpfApplication1.Model
{
    public class BookAuthor : INotifyPropertyChanged
    {
        private string _name;
        private string _photo;
        private DateTime _birthDate;
        public string Name
        {
            get { return _name; }
            set
            {
                if (_name != value)
                {
                    if (value != "")
                    {
                        _name = value;
                        OnPropertyChanged("Name");
                    }
                }
            }
        }
        public string Photo
        {
            get { return _photo; }
            set
            {
                if (_photo != value)
                {
                    _photo = value;
                    OnPropertyChanged("Photo");
                }
            }
        }
        public DateTime BirthDate
        {
            get { return _birthDate; }
            set
            {
                if (_birthDate != value)
                {
                    _birthDate = value;
                    OnPropertyChanged("BirthDate");
                }
            }
        }
        public MyCollection<Book> BooksList { get; set; }  = new MyCollection<Book>();

        public BookAuthor (string name, DateTime birthDate, string photo = "D:\\unknown.jpg")
        {
            this.Name = name;
            this.Photo = photo;
            this.BirthDate = birthDate;
        }

        public BookAuthor()
        {

        }
        public void AddBook(Book book)
        {
            this.BooksList.Add(book);
        }

        public override bool Equals(object other)
        {
            if (other is BookAuthor)
            {
                return Equals((BookAuthor)other);
            }
            return false;
        }

        public override int GetHashCode()
        {
            return Name.GetHashCode() * BirthDate.GetHashCode();
        }



        public bool Equals(BookAuthor other)
        {
            if ((object)other == null)
            {
                return false;
            }
            if (this.Name == other.Name && this.BirthDate == other.BirthDate)
                return true;
            else
                return false;
        }

        public event PropertyChangedEventHandler PropertyChanged;

        private void OnPropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
