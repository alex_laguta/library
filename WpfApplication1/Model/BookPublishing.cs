﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;

namespace WpfApplication1.Model
{
    public class BookPublishing : INotifyPropertyChanged
    {
        private string _name;
        private string _city;
        public string Name
        {
            get { return _name; }
            set
            {
                if (_name != value)
                {
                    if (value != "")
                    {
                        _name = value;
                        OnPropertyChanged("Name");
                    }
                }
            }
        }
        public string City
        {
            get { return _city; }
            set
            {
                if (_city != value)
                {
                    _city = value;
                    OnPropertyChanged("City");
                }
            }
        }
        public MyCollection<Book> BooksList { get; set; } = new MyCollection<Book>();

        public BookPublishing (string name, string city)
        {
            this.Name = name;
            this.City = city;
        }

        public BookPublishing()
        {

        }

        public void AddBook (Book book)
        {
            this.BooksList.Add(book);
        }

        public bool Equals(BookPublishing other)
        {
            if ((object)other == null)
            {
                return false;
            }
            if (this.Name == other.Name && this.City == other.City)
                return true;
            else
                return false;
        }


        public event PropertyChangedEventHandler PropertyChanged;

        private void OnPropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
