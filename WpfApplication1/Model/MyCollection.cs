﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WpfApplication1.Model
{
    public class MyCollection<T> : IList<T>, INotifyCollectionChanged
    {
        public event NotifyCollectionChangedEventHandler CollectionChanged;
          public void OnCollectionChanged(NotifyCollectionChangedEventArgs args)
          {
              var handler = CollectionChanged;
              if (handler != null)
                      handler(this, args);
          }
        private T[] _items = new T[0];
        public int Count
        {
            get { return _items.Length; }
        }

        public T this[int index]
        {
            get { return _items[index]; } 
            set { _items[index] = value; }
        }

        public void Add (T item)
        {
            Array.Resize(ref _items, Count + 1);
            _items[Count - 1] = item;
            OnCollectionChanged(new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Add, item, Count - 1));
        }

       public void AddByIndex(T item, int index)
        {
            Array.Resize(ref _items, Count + 1);
            for (int i = Count - 2; i > index - 1; i--)
                _items[i+1] = _items[i];
            _items[index] = item;
            OnCollectionChanged(new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Add, item, index));
        }

        public void Clear()
        {
            Array.Resize(ref _items, 0);
            OnCollectionChanged(new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Reset));
        }
        public int IndexOf (T item)
        {
            for (int i = 0; i < Count; i++)
            {
                if (item.Equals(_items[i]))
                    return i;
            }
            return -1;
        }

        public bool Contains(T item)
        {
            for (int i = 0; i < Count; i++)
            {
                if (item.Equals(_items[i]))
                    return true;
            }
            return false;
        }

        public void Insert(int index, T item)
        {
            Array.Resize<T>(ref _items, Count + 1);
            for (int i = Count - 1; i > index; i--)
                _items[i] = _items[i - 1];
            _items[index] = item;
        } 

        public bool IsReadOnly
        {
            get { return false; }
        }

        public bool Remove (T item)
        {
            if (IndexOf(item) != -1)
            {
                RemoveAt(IndexOf(item));
                return true;
            }
            else return false;
        }

        public void RemoveAt (int index)
        {
            T item = _items[index];
            for (int i = index; i < Count - 1; i++)
                _items[i] = _items[i + 1];
            Array.Resize(ref _items, Count - 1);
            OnCollectionChanged(new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Remove, item, index));
        }

        public void CopyTo (T[] container, int index)
        {
            int j = index;
            for (int i = 0; i < Count; i++)
            {
                container.SetValue(_items[i], j);
                j++;
            }
        }

        public void CopyTo(MyCollection<T> container)
        {
            for (int i = 0; i < Count; i++)
            {
                container.Add(this[i]);
            }
        }

        public void MoveTo(T item, int nIndex)
        {
            T newItem;
            newItem = item;
            Remove(item);
            AddByIndex(newItem, nIndex);
        }

        public IEnumerator<T> GetEnumerator()
        {
            for (int i = 0; i < _items.Length; ++i)
                yield return _items[i];
        }
        IEnumerator IEnumerable.GetEnumerator()
        {
            for (int i = 0; i < _items.Length; ++i)

                yield return _items[i];
        }


    }

}
