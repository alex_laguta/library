﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;

namespace WpfApplication1.Model
{
    public class Book : INotifyPropertyChanged
    {
        private string _name;
        private long _ISBN;
        private int _pagesQuantity;
        private int _publishingYear;
        private BookPublishing _publisher;

        public string Name
        {
          get { return _name; }
          set
            {
                if (_name != value)
                {
                    if (value != "")
                    {
                        _name = value;
                        OnPropertyChanged("Name");
                    }
                }
            }
        }
        public long ISBN
        {
            get { return _ISBN; }
            set
            {
                if (_ISBN != value)
                {
                    _ISBN = value;
                    OnPropertyChanged("ISBN");
                }
            }
        }
        public MyCollection<BookAuthor> AuthorsList { get; set; } = new MyCollection<BookAuthor>();
        public int PagesQuantity
        {
            get { return _pagesQuantity; }
            set
            {
                if (_pagesQuantity != value)
                {
                    _pagesQuantity = value;
                    OnPropertyChanged("PagesQuantity");
                }
            }
        }
        public MyCollection<string> TagsList { get; set; } = new MyCollection<string>();
        public int PublishingYear
        {
            get { return _publishingYear; }
            set
            {
                if (_publishingYear != value)
                {
                    if (value <= 2016)
                    {
                        _publishingYear = value;
                        OnPropertyChanged("PublishingYear");
                    }
                }
            }
        }
        public BookPublishing Publisher
        {
            get { return _publisher; }
            set
            {
                if (_publisher != value)
                {
                    _publisher = value;
                    OnPropertyChanged("Publisher");
                }
            }
        }

        public Book (long ISBNnumber, int pagesQuantity, int publishingYear, BookPublishing publisher, string name = "Untitled")
        {
            this.Name = name;
            this.ISBN = ISBNnumber;
            this.PagesQuantity = pagesQuantity;
            this.PublishingYear = publishingYear;
            this.Publisher = publisher;
        }

        public Book(string name, string publisher)
        {
            this.Name = name;
            this.Publisher = new BookPublishing(publisher, "\0");
        }
        public void AddAuthor(BookAuthor author)
        {
            this.AuthorsList.Add(author);
        }

        public Book()
        {

        }

        public void AddTag(string tag)
        {
            this.TagsList.Add(tag);
        }
        public override bool Equals(object other)
        {
            if (other is Book)
            {
                return Equals((Book)other);
            }
            return false;
        }

        public override int GetHashCode()
        {
            return Name.GetHashCode() * ISBN.GetHashCode();
        }
        public bool Equals(Book other)
        {
            if ((object)other == null)
            {
                return false;
            }

            if (this.Name == other.Name && this.ISBN == other.ISBN && this.PagesQuantity == other.PagesQuantity && this.PublishingYear == other.PublishingYear)
                return true;
            else
                return false;
        }

        public event PropertyChangedEventHandler PropertyChanged;

        private void OnPropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

    }
}
